fn main() {
    //binding with range patter
    let x = 1;
    match x {
        e @ 1...5 => println!("got a range element {}", e),
        _ => println!("anything"),
    }
    //binding with destructuring pattern_bindings
    struct Point {
        x:i32,
        y:i32
    }
    let origin = Point{x:0,y:0};
    match origin {
        Point{x,y} if x == y => println!("got a median point with x:{} and y:{}",x,y),
        _ => println!("anything"),
    }
}
